extern crate rand;
extern crate sdl2;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use std::env;
use std::fs::File;
use std::io;
use std::io::Read;
use std::time::Duration;

static WIDTH: u32 = 1024;
static HEIGHT: u32 = 512;

static FONTSET: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80, // F
];

static KEYMAP: [sdl2::keyboard::Keycode; 16] = [
    Keycode::Num1,
    Keycode::Num2,
    Keycode::Num3,
    Keycode::Num4,
    Keycode::Q,
    Keycode::W,
    Keycode::E,
    Keycode::R,
    Keycode::A,
    Keycode::S,
    Keycode::D,
    Keycode::F,
    Keycode::Z,
    Keycode::X,
    Keycode::C,
    Keycode::V,
];

pub struct Chip8 {
    opcode: u16,
    memory: [u8; 4096],
    v: [u8; 16],           // registers
    i: u16,                // index register
    pc: u16,               // program counter
    gfx: [[bool; 64]; 32], // screen state
    delay_timer: u8,
    sound_timer: u8,
    stack: [u16; 16],
    sp: u16, // stack pointer
    draw_flag: bool,
    key: [bool; 16], // state of the keys
}

impl Chip8 {
    pub fn new() -> Chip8 {
        let mut chip8 = Chip8 {
            opcode: 0,
            memory: [0; 4096],
            v: [0; 16],
            i: 0,
            pc: 0x200, // 512, start of the program counter
            gfx: [[false; 64]; 32],
            delay_timer: 0,
            sound_timer: 0,
            stack: [0; 16],
            sp: 0,
            draw_flag: true,
            key: [false; 16],
        };

        // copy fontset to memory
        for i in 0..80 {
            chip8.memory[i] = FONTSET[i];
        }

        return chip8;
    }

    pub fn load(&mut self, filename: String) -> Result<(), io::Error> {
        let mut buffer = Vec::new();
        let mut file = File::open(filename)?;

        file.read_to_end(&mut buffer)?;

        for (i, b) in buffer.iter().enumerate() {
            self.memory[i + 512] = *b;
        }
        Ok(())
    }

    pub fn emulate_cycle(&mut self) {
        // fetch opcode
        self.opcode = (self.memory[self.pc as usize] as u16) << 8
            | self.memory[(self.pc + 1) as usize] as u16;

        // decode opcode and execute
        match self.opcode & 0xF000 {
            0x0000 => self.opcode_0(),
            0x1000 => self.opcode_1(),
            0x2000 => self.opcode_2(),
            0x3000 => self.opcode_3(),
            0x4000 => self.opcode_4(),
            0x5000 => self.opcode_5(),
            0x6000 => self.opcode_6(),
            0x7000 => self.opcode_7(),
            0x8000 => self.opcode_8(),
            0x9000 => self.opcode_9(),
            0xA000 => self.opcode_a(),
            0xB000 => self.opcode_b(),
            0xC000 => self.opcode_c(),
            0xD000 => self.opcode_d(),
            0xE000 => self.opcode_e(),
            0xF000 => self.opcode_f(),
            _ => eprintln!("Unknown opcode: {:#06x}", self.opcode),
        }; // TODO replace by function lookup

        // update timers
        if self.delay_timer > 0 {
            self.delay_timer -= 1;
        }
        if self.sound_timer > 0 {
            if self.sound_timer == 1 {
                println!("Beep!"); // TODO
            }
            self.sound_timer -= 1;
        }
    }

    fn opcode_0(&mut self) {
        match self.opcode & 0x0FFF {
            // OOEO: clears the screen
            0x00E0 => {
                self.gfx = [[false; 64]; 32];
            }
            // 00EE: returns from subroutine
            0x00EE => {
                self.sp -= 1;
                self.pc = self.stack[self.sp as usize];
            }
            _ => eprintln!(
                "RCA 1802 instructions are not implemented: {:#06x}",
                self.opcode
            ),
        }
        self.pc += 2;
    }

    // 1NNN: goto address NNN
    fn opcode_1(&mut self) {
        self.pc = self.opcode & 0x0FFF;
    }

    // 2NNN: calls subroutine at NNN
    fn opcode_2(&mut self) {
        self.stack[self.sp as usize] = self.pc;
        self.sp += 1;
        self.pc = self.opcode & 0x0FFF;
    }

    // 3XNN: skips next instruction if VX == NN
    fn opcode_3(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        if self.v[x as usize] == (self.opcode & 0x00FF) as u8 {
            self.pc += 4;
        }
        self.pc += 2;
    }

    // 4XNN: skips next instruction if VX != NN
    fn opcode_4(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        if self.v[x as usize] != (self.opcode & 0x00FF) as u8 {
            self.pc += 2;
        }
        self.pc += 2;
    }

    // 5XY0: skips next instruction if VX == VY
    fn opcode_5(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        let y = (self.opcode & 0x00F0) >> 4;
        if self.v[x as usize] == self.v[y as usize] {
            self.pc += 2;
        }
        self.pc += 2;
    }

    // 6XNN: sets VX to NN
    fn opcode_6(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        self.v[x as usize] = (self.opcode & 0x00FF) as u8;
        self.pc += 2;
    }

    // 7XNN: adds NN to VX (no change to carry flag)
    fn opcode_7(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        self.v[x as usize] = self.v[x as usize].wrapping_add((self.opcode & 0x00FF) as u8);
        self.pc += 2;
    }

    // 8XYN: registers operations
    fn opcode_8(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        let y = (self.opcode & 0x00F0) >> 4;
        match self.opcode & 0x000F {
            // 8XY0: sets VX to the value of VY
            0x0000 => {
                self.v[x as usize] = self.v[y as usize];
            }
            // 8XY1: sets VX to (VX | VY)
            0x0001 => {
                self.v[x as usize] = self.v[x as usize] | self.v[y as usize];
            }
            // 8XY2: sets VX to (VX & VY)
            0x0002 => {
                self.v[x as usize] = self.v[x as usize] & self.v[y as usize];
            }
            // 8XY3: sets VX to (VX ^ VY)
            0x0003 => {
                self.v[x as usize] = self.v[x as usize] ^ self.v[y as usize];
            }
            // 8XY4: adds VY to VX
            0x0004 => {
                let (res, carry) = self.v[x as usize].overflowing_add(self.v[y as usize]);
                // VF is set to 1 when there is a carry
                self.v[0xF] = if carry { 1 } else { 0 };
                // and to 0 when there is not
                self.v[x as usize] = res;
            }
            // 8XY5: substracts VY from VX
            0x0005 => {
                let (res, borrow) = self.v[x as usize].overflowing_sub(self.v[y as usize]);
                // VF is set to 1 when there is a borrow
                self.v[0xF] = if borrow { 1 } else { 0 };
                // and to 0 when there is not
                self.v[x as usize] = res;
            }
            // 8XY6: shifts VX to the right by 1
            0x0006 => {
                // stores the least significant bit of VX in VF
                self.v[0xF] = (self.v[x as usize] % 100) % 10;
                self.v[x as usize] = self.v[x as usize].wrapping_shr(1);
            }
            // 8XY7: sets VX to VY - VX
            0x0007 => {
                let (res, borrow) = self.v[y as usize].overflowing_sub(self.v[x as usize]);
                // VF is set to 1 when there is a borrow
                self.v[0xF] = if borrow { 1 } else { 0 };
                // and to 0 when there is not
                self.v[x as usize] = res;
            }
            // 8XYE: shifts VX to the left by 1
            0x000E => {
                // stores the most significant bit of VX in VF
                self.v[0xF] = self.v[x as usize] / 100;
                self.v[x as usize] = self.v[x as usize].wrapping_shl(1);
            }
            _ => eprintln!("Unknown 8XYN opcode: {:#06x}", self.opcode),
        };
        self.pc += 2;
    }

    // 9XY0: skips next instruction if VX != VY
    fn opcode_9(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        let y = (self.opcode & 0x00F0) >> 4;
        if self.v[x as usize] != self.v[y as usize] {
            self.pc += 2;
        }
        self.pc += 2;
    }

    // ANNN: sets I to the address NNN
    fn opcode_a(&mut self) {
        self.i = self.opcode & 0x0FFF;
        self.pc += 2;
    }

    // BNNN: goto address NNN + V0
    fn opcode_b(&mut self) {
        self.pc = (self.opcode & 0x0FFF) + self.v[0x0] as u16;
    }

    // CXNN: sets VX to rand & NN
    fn opcode_c(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        let random: u8 = rand::random::<u8>();
        self.v[x as usize] = random & (self.opcode & 0x00FF) as u8;
        self.pc += 2;
    }

    // DXYN: draws a 8xN pixels sprite at (VX, VY)
    fn opcode_d(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        let y = (self.opcode & 0x00F0) >> 4;
        let height = (self.opcode & 0x000F) as u8;
        let mut pixel: u8;

        // VF will be set to 1 if any screen pixels are flipped from set to unset
        self.v[0xF] = 0;
        for yline in 0..height {
            pixel = self.memory[(self.i + yline as u16) as usize];
            for xline in 0..8 {
                // check if current evaluated pixel is set to 1
                if pixel & (0x80 >> xline) != 0 {
                    let coord_x = (self.v[x as usize] + xline) % 64;
                    let coord_y = (self.v[y as usize] + yline) % 32;
                    // check if pixel on the display is set to 1
                    if self.gfx[coord_y as usize][coord_x as usize] {
                        // register the collision
                        self.v[0xF] = 1;
                    }
                    // set the pixel value
                    self.gfx[coord_y as usize][coord_x as usize] ^= true;
                }
            }
        }

        self.draw_flag = true;
        self.pc += 2;
    }

    // EXNN: skips next instruction depending on input
    fn opcode_e(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        match self.opcode & 0x00FF {
            0x009E => {
                if self.key[self.v[x as usize] as usize] {
                    self.pc += 2;
                } // skips if key is pressed
            }
            0x00A1 => {
                if !self.key[self.v[x as usize] as usize] {
                    self.pc += 2;
                } // skips if key is not pressed
            }
            _ => eprintln!("Unknown EXNN opcode: {:#06x}", self.opcode),
        }
        self.pc += 2;
    }

    // FXNN
    fn opcode_f(&mut self) {
        let x = (self.opcode & 0x0F00) >> 8;
        match self.opcode & 0x00FF {
            // FX07: sets VX to the value of the delay timer
            0x0007 => {
                self.v[x as usize] = self.delay_timer;
            }
            // FX0A: awaits a key press, then stores it in VX
            0x000A => {
                for (key, pressed) in self.key.iter().enumerate() {
                    if *pressed {
                        self.v[x as usize] = key as u8;
                        self.pc += 2; // advance normally
                        break;
                    }
                }
                // stays on current instruction if no key was pressed
                self.pc -= 2;
            }
            // FX15: sets the delay timer to VX
            0x0015 => {
                self.delay_timer = self.v[x as usize];
            }
            // FX18: sets the sound timer to VX
            0x0018 => {
                self.sound_timer = self.v[x as usize];
            }
            // FX1E: adds VX to I
            0x001E => {
                let (res, overflow) = self.i.overflowing_add(self.v[x as usize].into());
                // sets VF to 1 when there is a range overflow, 0 otherwise
                self.v[0xF] = if overflow { 1 } else { 0 };
                self.i = res;
            }
            // FX29: sets I to the location of the sprite for the character in VX
            0x0029 => {
                // multiply character by sprite length
                self.i = (0x5 * self.v[x as usize]).into();
            }
            // FX33: stores the binary-coded decimal representation of VX
            0x0033 => {
                // stores the most significant digit
                self.memory[self.i as usize] = self.v[x as usize] / 100;
                // stores the middle digit
                self.memory[(self.i + 1) as usize] = (self.v[x as usize] / 10) % 10;
                // stores the least significant digit
                self.memory[(self.i + 2) as usize] = (self.v[x as usize] % 100) % 10;
            }
            // FX55: stores V0 to VX included in memory starting at address I
            0x0055 => {
                for n in 0..(x + 1) {
                    self.memory[(self.i + n) as usize] = self.v[n as usize];
                }
                // I itself is not modified
            }
            // FX65: fills V0 to VX included with values in memory starting from address I
            0x0065 => {
                for n in 0..(x + 1) {
                    self.v[n as usize] = self.memory[(self.i + n) as usize];
                }
                // I itself is not modified
            }
            _ => eprintln!("Unknown FXNN opcode: {:#06x}", self.opcode),
        }
        self.pc += 2;
    }

    fn draw_graphics(&mut self) -> Vec<Rect> {
        self.draw_flag = false;
        let w: u32 = WIDTH / 64;
        let h: u32 = HEIGHT / 32;
        let mut points: Vec<Rect> = Vec::new();
        for y in 0..32 {
            for x in 0..64 {
                if self.gfx[y][x] {
                    points.push(Rect::new(x as i32 * w as i32, y as i32 * h as i32, w, h));
                }
            }
        }
        return points;
    }
}

fn main() -> Result<(), io::Error> {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        eprintln!("Please provide the ROM file name to load as an argument");
        return Ok(());
    }

    let filename = &args[1];

    // setup graphics
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    // x5 scaling from 64x32
    let window = video_subsystem
        .window("Cheap8", WIDTH, HEIGHT)
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().present_vsync().build().unwrap();

    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    // setup input

    // setup emulator
    let mut chip8 = Chip8::new();

    // load ROM
    chip8.load(filename.to_string())?;

    // main loop
    let mut event_pump = sdl_context.event_pump().unwrap();
    'running: loop {
        chip8.emulate_cycle();

        if chip8.draw_flag {
            canvas.set_draw_color(Color::RGB(0, 0, 0));
            canvas.clear();
            let points = chip8.draw_graphics();
            canvas.set_draw_color(Color::RGB(255, 255, 255));
            canvas.fill_rects(points.as_slice()).unwrap();
            canvas.present();
        }

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                Event::KeyDown {
                    keycode: Some(keycode),
                    ..
                } => {
                    for (i, key) in KEYMAP.iter().enumerate() {
                        if keycode == *key {
                            chip8.key[i] = true;
                        }
                    }
                }
                Event::KeyUp {
                    keycode: Some(keycode),
                    ..
                } => {
                    for (i, key) in KEYMAP.iter().enumerate() {
                        if keycode == *key {
                            chip8.key[i] = false;
                        }
                    }
                }
                _ => {}
            }
        }

        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
    Ok(())
}
